@extends('layouts.app')

@section('content')
    <math-results-component
            :math-data="{{ json_encode($mathData) }}"
    ></math-results-component>
@endsection