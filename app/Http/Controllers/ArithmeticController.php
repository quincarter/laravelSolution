<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArithmeticController extends Controller
{
    protected $result = 0;

    public function index()
    {
        return view('math.index');
    }

    public function math(Request $request)
    {
        //var_dump($request);
        $val1 = (int)$request->value1;
        $val2 = (int)$request->value2;

        $mathData = $this->result = $this->multiply($val1, $val2);

        return view('math.results', compact('mathData'));
    }

    private function multiply(int $val1, int $val2)
    {
        if (intval($val1) && intval($val2)) {
            if ($val2 === 0) {
                return 0;
            } else if ($val2 > 0) {
                return ($val1 + $this->multiply($val1, $val2 - 1));
            } else if ($val1 < 0 || $val2 < 0) {
                return -($this->multiply($val1, -$val2));
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}